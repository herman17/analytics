# Start the ROS kernel
source /opt/ros/noetic/setup.bash

# Prevent "Unable to init server: Could not connect: Connection refused" from matplotlib
export DISPLAY=:0.0
export MPLBACKEND='Agg'

# Run the Watcher
cd /var/www/vhosts/pedantic-babbage.85-214-137-134.plesk.page/Analytics/Source
/usr/bin/python3 watcher.py
