# Receive flight data uploaded by a drone into the telemetry data store.

import datetime
import sys
# Local modules
import bag2csv
from config import Configuration as cfg
from logger import Logger


class Receiver:

    # Receive one flight. (The same Receiver object can be reused to receive multiple flights.)
    # upload_dir: directory where flight data has been uploaded: {data_dir}/upload/{drone-id}_{datetime}
    # returns telemetry_dir: directory of the flight data after receiving: {data_dir}/telemetry/{drone-id}/{date}/{time}
    # where {data_dir} is the root directory of all data, e.g. a mount point or a local directory.
    def receive(self, upload_dir):
        logger = Logger('receiver')
        logger.info(upload_dir, indent=0)

        # Parse metadata
        drone_id = upload_dir.parent.parent.name
        timestamp = datetime.datetime.fromisoformat(upload_dir.name)

        # Figure out where to import the data
        date = timestamp.date().isoformat()
        time = timestamp.time().isoformat(timespec='seconds')
        telemetry_dir = cfg.telemetry_dir / drone_id / date / time

        # Create the telemetry directory
        telemetry_dir.mkdir(parents=True)
        file_types = {'bag', 'csv', 'txt'}
        for file_type in file_types:
            (telemetry_dir / file_type).mkdir()

        # Move expected files
        for file_type in file_types:
            for log_file in upload_dir.glob(f'*.{file_type}'):
                log_file.rename(telemetry_dir / file_type / log_file.name)
        metadata = upload_dir / 'metadata.json'
        if metadata.exists():
            metadata.rename(telemetry_dir / metadata.name)
        else:
            logger.warning('metadata.json is missing')

        # Remove the 'upload_done.txt' file
        upload_done = upload_dir / 'upload_done.txt'
        upload_done.unlink(missing_ok=True)

        # Move unexpected files and directories
        other_paths = upload_dir.glob('*')
        if other_paths:
            upload_dir.rename(telemetry_dir / 'other')
        else:
            upload_dir.rmdir()

        # Unpack bag files
        for bag_file in (telemetry_dir / 'bag').glob('*.bag'):
            stem = bag_file.stem
            if len(stem) > 20 and stem[-20] == '_' and all(char in '-0123456789' for char in stem[-19:]):
                stem = stem[:-20]  # Strip rosbag timestamp
            if stem.endswith('_json'):
                stem = stem[:-5]
            csv_file = telemetry_dir / 'csv' / (stem + '.csv')
            try:
                bag2csv.convert_file(bag_file, csv_file)
            except:
                logger.error(f'{bag_file} could not be unbagged')

        # Return where the flight's telemetry data is now stored
        return telemetry_dir
