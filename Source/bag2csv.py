import collections
import csv
import json
import rosbag


def convert_file(bag_file, csv_file):
    if 'json' in bag_file.stem:
        JsonConverter().convert(bag_file, csv_file)
    else:
        NativeConverter().convert(bag_file, csv_file)


class NativeConverter:

    def convert(self, bag_path, csv_path):
        bag = rosbag.Bag(bag_path)
        records = bag.read_messages()
        try:
            first_record = next(records)
        except StopIteration:
            return  # Empty bag file; no csv file produced
        with open(csv_path, 'w') as target:
            writer = csv.writer(target)
            writer.writerow([name for name, _ in self.convert_message(first_record.message)])
            writer.writerow([str(value) for _, value in self.convert_message(first_record.message)])
            for record in records:
                writer.writerow([str(value) for _, value in self.convert_message(record.message)])
            bag.close()

    def convert_message(self, msg):
        for name, type_ in zip(msg.__slots__, msg._slot_types):
            if name == 'layout' and type_ == 'std_msgs/MultiArrayLayout':
                continue  # Skip array layouts; they are metadata not data
            value = getattr(msg, name)
            for name_csv_pair in self.convert_value(name, type_, value):
                yield name_csv_pair

    def convert_value(self, name, type_, value):
        if '/' in type_:
            for name_csv_pair in self.convert_struct(name, type_, value):
                yield name_csv_pair
        elif type_.endswith('[]'):
            array_type = type_[:-2]
            for name_csv_pair in self.convert_array(name, array_type, value):
                yield name_csv_pair
        else:  # It's a primitive value
            yield self.convert_primitive(name, type_, value)

    def convert_struct(self, struct_name, struct_type, struct_value):
        for field_name, field_value in self.convert_message(struct_value):
            qualified_field_name = '%s.%s' % (struct_name, field_name)
            yield qualified_field_name, field_value

    def convert_array(self, array_name, array_type, array_value):
        for index, element_value in enumerate(array_value):
            element_name = '%s[%d]' % (array_name, index)
            element_csv = self.format(array_type, element_value)
            yield element_name, element_csv

    def convert_primitive(self, name, type_, value):
        csv = self.format(type_, value)
        return name, csv

    def format(self, type_, value):
        if type_ == 'bool':
            csv = 'TRUE' if value else 'FALSE'           # Spreadsheets use all caps
        elif type_ == 'float32':
            csv = '%0.7g' % value                        # Round to 7 significant digits
        elif type_ == 'time':
            csv = '%d.%09d' % (value.secs, value.nsecs)  # Preserve nanoseconds
        else:
            csv = str(value)                             # All other value types
        return csv


class JsonConverter:

    def convert(self, bag_path, csv_path):
        bag = rosbag.Bag(bag_path)
        records = bag.read_messages()
        try:
            first_record = next(records)
        except StopIteration:
            return  # Empty bag file; no csv file produced
        assert first_record.message.__slots__ == ['data']
        assert first_record.message._slot_types == ['string']
        with open(csv_path, 'w') as target:
            writer = csv.writer(target)
            writer.writerow([name for name, _ in self.convert_message(first_record.message)])
            writer.writerow([str(value) for _, value in self.convert_message(first_record.message)])
            for record in records:
                writer.writerow([str(value) for _, value in self.convert_message(record.message)])
            bag.close()

    def convert_message(self, msg):
        data = json.loads(msg.data, object_pairs_hook=collections.OrderedDict)
        for name_csv_pair in self.convert_value('', data):
            yield name_csv_pair

    def convert_value(self, name, value):
        if isinstance(value, dict):
            if name == 'header.stamp' and len(value) == 2 and 'secs' in value and 'nsecs' in value:
                # It's a time value, which ROS considers a primitive type
                yield self.convert_primitive(name, value)
            else:
                for name_csv_pair in self.convert_object(name, value):
                    yield name_csv_pair
        elif isinstance(value, list):
            for name_csv_pair in self.convert_array(name, value):
                yield name_csv_pair
        else:
            yield self.convert_primitive(name, value)

    def convert_object(self, object_name, object_value):
        for member_name, member_value in object_value.items():
            qualified_member_name = '%s.%s' % (object_name, member_name) if object_name else member_name
            for name_csv_pair in self.convert_value(qualified_member_name, member_value):
                yield name_csv_pair

    def convert_array(self, array_name, array_value):
        for index, element_value in enumerate(array_value):
            element_name = '%s[%d]' % (array_name, index)
            for name_csv_pair in self.convert_value(element_name, element_value):
                yield name_csv_pair

    def convert_primitive(self, name, value):
        csv = self.format(value)
        return name, csv

    def format(self, value):
        if isinstance(value, bool):
            csv = 'TRUE' if value else 'FALSE'           # Spreadsheets use all caps
        elif isinstance(value, float):
            csv = '%0.7g' % value                        # Round to 7 significant digits
        elif isinstance(value, dict):
            # It's a time value, which ROS considers a primitive type
            csv = '&%d.%09d' % (value['secs'], value['nsecs'])  # Preserve nanoseconds
        else:
            csv = str(value)                             # All other value types
        return csv
