import backports.zoneinfo as zoneinfo
import collections
import csv
import datetime
import json
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
import pathlib
import tf

from config import Configuration as cfg


class Metadata:

    def __init__(self, filename):
        if filename.exists():
            with open(filename) as source:
                self.jsondata = json.load(source)
        else:
            self.jsondata = None

    def __getitem__(self, keys):
        data = self.jsondata
        for key in keys:
            if not data: return
            data = data.get(key)
        return data


class FlightReport:

    def report(self, telemetry_dir):
        if telemetry_dir.name.startswith('Flight_'):
            telemetry_dir = telemetry_dir.rename(telemetry_dir.parent / telemetry_dir.name[7:])
        self.telemetry_dir = telemetry_dir      # Where drone data has been uploaded

        # Figure out where to write the flight report
        drone_id = telemetry_dir.parent.parent.name
        workday = telemetry_dir.parent.name
        flight_time = telemetry_dir.name
        self.analytics_dir = cfg.analytics_dir / 'Flights' / drone_id / workday / flight_time
        self.analytics_dir.mkdir(parents=True)  # Should not already exist

        self.read_metadata()
        self.read_data_series()
        self.write_key_values()
        self.fix_time_series()
        self.fix_quaternions()
        self.generate_signals()
        self.plot_graphs()
        self.write_report()

    def read_metadata(self):
        self.metadata = Metadata(self.telemetry_dir / 'metadata.json')

    def write_key_values(self):
        self.key_values = {}
        # From metadata
        self.key_values['drone_id'] = self.metadata['drone', 'id']
        self.key_values['drone_name'] = self.metadata['drone', 'name']
        self.key_values['location_id'] = self.metadata['location', 'id']
        self.key_values['location_name'] = self.metadata['location', 'name']
        self.key_values['organization_id'] = self.metadata['organization', 'id']
        self.key_values['organization_name'] = self.metadata['organization', 'name']
        # From data series
        self.key_values['battery_used'] = self.calc_battery_used()
        self.key_values['flight_duration'] = self.calc_flight_duration()
        self.key_values['flight_takeoff'] = self.calc_flight_takeoff()
        self.key_values['images_taken'] = self.calc_images_taken()
        self.key_values['max_accuracy'] = self.calc_max_accuracy()
        self.key_values['max_cpu_temp'] = self.calc_max_cpu_temp()
        # Write to disk
        file = self.analytics_dir / 'key_values.csv'
        with open(file, 'w') as target:
            writer = csv.writer(target)
            writer.writerow(['key', 'value'])
            for key in sorted(self.key_values):
                value = self.key_values[key]
                writer.writerow([key, value])

    def generate_signals(self):
        signals = [('parameter', 'value', 'description')]
        # CPU temperature
        max_cpu_temp = self.key_values['max_cpu_temp']
        if max_cpu_temp and max_cpu_temp > 80.0:
            signals.append(('max_cpu_temp', max_cpu_temp, 'CPU temperature running too high'))
        # Write to disk
        file = self.analytics_dir / 'signals.csv'
        with open(file, 'w') as target:
            writer = csv.writer(target)
            writer.writerows(signals)

    def read_data_series(self):
        self.data_series = {}
        self.read_accuracy_data()
        self.read_battery_data()
        self.read_corvus_pos_data()
        self.read_cpu_data()
        self.read_image_data()
        self.read_marker_data()
        self.read_nav_state_data()
        self.read_voxl_pos_data()

    # Some of the time series are Unix epoch based; others use the drone startup
    # time as epoch. What we want is 'mission time' measured from takeoff.
    # See 'mission_time.txt' for more information.
    # Also notice that while the drone records timestamps in UTC, Analytics runs
    # in zoned time (MET, i.e. UTC+0100 or UTC+0200, depending on the weather)
    def fix_time_series(self):
        def fix(series, seconds):
            if series := self.data_series.get(series):
                series['time'] = [time - seconds for time in series['time']]
        # Time constants (all in seconds)
        c = int(self.metadata[('time_offset_ns',)] or 0) / 1.0e9
        t = self.calc_flight_takeoff().timestamp()
        a = t - c
        # Fix time series in Unix time
        fix('accuracy', a)
        fix('corvus_pos', a)
        fix('nav_state', a)
        fix('voxl_pos', a)
        # Time shift (in seconds) for the drone boot epoch
        fix('battery', t)
        fix('cpu', t)
        fix('markers', t)

    # Convert quaternions to Euler angles.
    def fix_quaternions(self):
        def fix(series):
            if series := self.data_series.get(series):
                series['rpy'] = [tf.transformations.euler_from_quaternion(q) for q in series['ori']]
        fix('corvus_pos')
        fix('voxl_pos')

    def read_accuracy_data(self):
        file = self.telemetry_dir / 'csv'/ 'corvus_position_estimator_accuracy.csv'
        if file.exists():
            with open(file) as source:
                reader = csv.reader(source)
                if next(reader) == ['voxl_timestamp', 'accuracy_xy', 'accuracy_yaw', 'accuracy_z']:
                    time, xy, yaw, z = [], [], [], []
                    count = 0
                    for row in reader:
                        time.append(int(row[0]) / 1.0e9)         # second
                        xy.append(float(row[1]))                 # meter
                        yaw.append(math.degrees(float(row[2])))  # degrees
                        z.append(float(row[3]))                  # meter
                        count += 1
                    if count > 0:
                        self.data_series['accuracy'] = {'time': time, 'xy': xy, 'yaw': yaw, 'z': z}

    def read_battery_data(self):
        file = self.telemetry_dir / 'csv'/ 'mavros_battery.csv'
        if file.exists():
            with open(file) as source:
                reader = csv.reader(source)
                if next(reader) == ['header.seq', 'header.stamp', 'header.frame_id', 'voltage', 'current', 'charge', 'capacity', 'design_capacity', 'percentage', 'power_supply_status', 'power_supply_health', 'power_supply_technology', 'present', 'cell_voltage[0]', 'cell_voltage[1]', 'cell_voltage[2]', 'location', 'serial_number']:
                    time, voltage, current, charge = [], [], [], []
                    count = 0
                    for row in reader:
                        time.append(float(row[1]))      # second
                        voltage.append(float(row[3]))   # volt
                        current.append(-float(row[4]))  # ampere
                        charge.append(float(row[8]))    # 0..1 (empty..full)
                        count += 1
                    if count >= 2:
                        self.data_series['battery'] = {'time': time, 'voltage': voltage, 'current': current, 'charge': charge}

    def read_corvus_pos_data(self):
        file = self.telemetry_dir / 'csv' / 'corvus_pose_fixed.csv'
        if file.exists():
            with open(file) as source:
                reader = csv.reader(source)
                if next(reader) == ['header.seq', 'header.stamp', 'header.frame_id', 'pose.position.x', 'pose.position.y', 'pose.position.z', 'pose.orientation.x', 'pose.orientation.y', 'pose.orientation.z', 'pose.orientation.w']:
                    time, pos, ori = [], [], []
                    count = 0
                    for row in reader:
                        time.append(float(row[1]))  # second
                        pos.append((float(row[3]), float(row[4]), float(row[5])))  # meter
                        ori.append((float(row[6]), float(row[7]), float(row[8]), float(row[9])))  # unit quaternion
                        count += 1
                    if count >= 2:
                        self.data_series['corvus_pos'] = {'time': time, 'pos': pos, 'ori': ori}

    def read_cpu_data(self):
        file = self.telemetry_dir / 'csv' / 'corvus_cpu.csv'
        if file.exists():
            with open(file) as source:
                reader = csv.reader(source)
                if next(reader) == ['time_ns', 'freq[0]', 'freq[1]', 'freq[2]', 'freq[3]', 'temp[0]', 'temp[1]', 'temp[2]', 'temp[3]', 'util[0]', 'util[1]', 'util[2]', 'util[3]']:
                    time = []
                    freq = [[] for cpu in range(4)]
                    temp = [[] for cpu in range(4)]
                    util = [[] for cpu in range(4)]
                    count = 0
                    for row in reader:
                        time.append(int(row[0]) / 1.0e9)
                        freq[0].append(float(row[1]))  # GHz
                        freq[1].append(float(row[2]))
                        freq[2].append(float(row[3]))
                        freq[3].append(float(row[4]))
                        temp[0].append(float(row[5]))  # °C
                        temp[1].append(float(row[6]))
                        temp[2].append(float(row[7]))
                        temp[3].append(float(row[8]))
                        util[0].append(float(row[9]))  # 0..1 (idle..fully occupied)
                        util[1].append(float(row[10]))
                        util[2].append(float(row[11]))
                        util[3].append(float(row[12]))
                        count += 1
                    if count > 0:
                        self.data_series['cpu'] = {'time': time, 'freq': freq, 'temp': temp, 'util': util}

    def read_image_data(self):
        file = self.telemetry_dir / 'csv' / 'corvus_payload_imagesTaken.csv'
        if file.exists():
            with open(file) as source:
                reader = csv.reader(source)
                if next(reader) == ['data']:
                    image_number = [int(row[0]) for row in reader]
                    if image_number:
                        self.data_series['images'] = image_number

    def read_marker_data(self):
        file = self.telemetry_dir / 'csv' / 'poscontrol_markers.csv'
        if file.exists():
            with open(file) as source:
                reader = csv.reader(source)
                if next(reader) == ['time', 'marker_timestamp', 'voxl_timestamp', 'id', 'status', 'x', 'y', 'z', 'horizontal_angle', 'vertical_angle', 'marker_distance', 'marker_distance_xy', 'marker_area']:
                    time, ident, status, x, y, z = [], [], [], [], [], []
                    count = 0
                    for row in reader:
                        time.append(int(row[0]) / 1.0e9)  # second
                        ident.append(row[3])
                        status.append(row[4])
                        x.append(float(row[5]) / 1.0e3)   # meter
                        y.append(float(row[6]) / 1.0e3)
                        z.append(float(row[7]) / 1.0e3)
                        count += 1
                    if count > 0:
                        self.data_series['markers'] = {'time': time, 'id': ident, 'status': status, 'x': x, 'y': y, 'z': z}

    def read_nav_state_data(self):
        file = self.telemetry_dir / 'csv'/ 'corvus_navigation_control_state_machine.csv'
        if file.exists():
            with open(file) as source:
                reader = csv.reader(source)
                header = next(reader)
                if header[0] == 'time_ns' and header[1] in {'event', 'state'}:  # fuckup: two possible headers
                    time, state = [], []
                    count = 0
                    for row in reader:
                        if row[1].startswith('S'):
                            time.append(int(row[0]) / 1.0e9)  # second
                            state.append(row[1])              # S_ARMING, S_STANDBY etc.
                            count += 1
                    if count >= 2:
                        self.data_series['nav_state'] = {'time': time, 'state': state}

    def read_voxl_pos_data(self):
        file = self.telemetry_dir / 'csv' / 'voxl_pose_fixed.csv'
        if file.exists():
            with open(file) as source:
                reader = csv.reader(source)
                if next(reader) == ['header.seq', 'header.stamp', 'header.frame_id', 'pose.position.x', 'pose.position.y', 'pose.position.z', 'pose.orientation.x', 'pose.orientation.y', 'pose.orientation.z', 'pose.orientation.w']:
                    time, pos, ori = [], [], []
                    count = 0
                    for row in reader:
                        time.append(float(row[1]))  # second
                        pos.append((float(row[3]), float(row[4]), float(row[5])))  # meter
                        ori.append((float(row[6]), float(row[7]), float(row[8]), float(row[9])))  # unit quaternion
                        count += 1
                    if count >= 2:
                        self.data_series['voxl_pos'] = {'time': time, 'pos': pos, 'ori': ori}

    def calc_battery_used(self):
        if data := self.data_series.get('battery'):
            charge = data['charge']
            battery_used = charge[-1] - charge[0]
            return battery_used

    def calc_flight_duration(self):
        if data := self.data_series.get('nav_state'):
            time = data['time']
            duration = time[-1] - time[0]
            return duration

    def calc_flight_takeoff(self):
        datestamp = self.telemetry_dir.parent.name
        date = datetime.date.fromisoformat(datestamp)
        timestamp = self.telemetry_dir.name
        time = datetime.time.fromisoformat(timestamp)
        flight_takeoff = datetime.datetime.combine(date, time, datetime.timezone.utc)
        return flight_takeoff

    def calc_images_taken(self):
        if data := self.data_series.get('images'):
            images_taken = max(data)
            return images_taken

    def calc_marker_visibility(self):
        if markers := self.data_series.get('markers'):
            markers_by_id = collections.defaultdict(list)
            for time, ident, status in zip(markers['time'], markers['id'], markers['status']):
                if status == 'okay!':
                    markers_by_id[ident].append(time)
            deltas = []
            for ident, times in markers_by_id.items():
                it = iter(sorted(times))
                interval_start = interval_stop = next(it)
                for time in it:
                    if time - interval_stop > 1.5:
                        deltas.append((interval_start, +1))
                        deltas.append((interval_stop + 1.0, -1))
                        interval_start = interval_stop = time
                    else:
                        interval_stop = time
                deltas.append((interval_start, +1))
                deltas.append((interval_stop + 1.0, -1))
            deltas.sort()
            time_seq, count_seq = [min(deltas[0][0], 0.0)], [0]
            for time, delta in deltas:
                time_seq.append(time)
                count_seq.append(count_seq[-1] + delta)
            return {'time': time_seq, 'count': count_seq}

    def calc_max_accuracy(self):
        if data := self.data_series.get('accuracy'):
            xy, z = data['xy'], data['z']
            max_accuracy = max(math.hypot(horz,vert) for horz, vert in zip(xy, z))
            return max_accuracy

    def calc_max_cpu_temp(self):
        if data := self.data_series.get('cpu'):
            temp = data['temp']  # 4 series for 4 CPUs
            max_temp = max(max(temp[cpu]) for cpu in range(4))
            return max_temp

    def plot_graphs(self):
        self.graph_dir = self.analytics_dir / 'graphs'
        self.graph_dir.mkdir()
        self.plot_accuracy()
        self.plot_alt_yaw()
        self.plot_battery()
        self.plot_cpu()
        self.plot_markers()
        self.plot_route()

    def plot_accuracy(self):
        if accuracy := self.data_series.get('accuracy'):
            fig, (ax_xy, ax_z, ax_yaw) = plt.subplots(1, 3, figsize=(12, 3.5))
            # xy
            ax_xy.set_title('x-y plane')
            ax_xy.set_xlabel('mission time [s]')
            ax_xy.set_ylabel('accuracy [m]')
            ax_xy.set_facecolor('#DDDDDD')
            ax_xy.plot(accuracy['time'], accuracy['xy'], color='red')
            ax_xy.set_xlim(left=0)
            ax_xy.set_ylim(bottom=0)
            # z
            ax_z.set_title('z height')
            ax_z.set_xlabel('mission time [s]')
            ax_z.set_ylabel('accuracy [m]')
            ax_z.set_facecolor('#DDDDDD')
            ax_z.plot(accuracy['time'], accuracy['z'], color='red')
            ax_z.set_xlim(left=0)
            ax_z.set_ylim(bottom=0)
            # yaw
            ax_yaw.set_title('yaw angle')
            ax_yaw.set_xlabel('mission time [s]')
            ax_yaw.set_ylabel('accuracy [°]')
            ax_yaw.set_facecolor('#DDDDDD')
            ax_yaw.plot(accuracy['time'], accuracy['yaw'], color='red')
            ax_yaw.set_xlim(left=0)
            ax_yaw.set_ylim(bottom=0)
            #
            fig.tight_layout(pad=2)
            plt.savefig(self.graph_dir / 'accuracy.png')
            plt.close(fig)

    def plot_alt_yaw(self):
        corvus_pos = self.data_series.get('corvus_pos')
        voxl_pos = self.data_series.get('voxl_pos')
        if corvus_pos or voxl_pos:
            fig, (ax_z, ax_yaw) = plt.subplots(1, 2, figsize=(12, 3.5))
            # Altitude
            ax_z.set_title('Altitude')
            ax_z.set_xlabel('mission time [s]')
            ax_z.set_ylabel('altitude [m]')
            ax_z.set_facecolor('#DDDDDD')
            if corvus_pos:
                z = [z for _, _, z in corvus_pos['pos']]
                ax_z.plot(corvus_pos['time'], z, label='corvus')
            if voxl_pos:
                z = [z for _, _, z in voxl_pos['pos']]
                ax_z.plot(voxl_pos['time'], z, label='voxl')
            ax_z.legend()
            # Yaw
            ax_yaw.set_title('Yaw')
            ax_yaw.set_xlabel('mission time [s]')
            ax_yaw.set_ylabel('yaw angle [deg]')
            ax_yaw.set_facecolor('#DDDDDD')
            if corvus_pos:
                yaw = [math.degrees(y) for _, _, y in corvus_pos['rpy']]
                ax_yaw.plot(corvus_pos['time'], yaw, label='corvus')
            if voxl_pos:
                yaw = [math.degrees(y) for _, _, y in voxl_pos['rpy']]
                ax_yaw.plot(voxl_pos['time'], yaw, label='voxl')
            ax_yaw.legend()
            #
            fig.tight_layout(pad=2)
            plt.savefig(self.graph_dir / 'alt_yaw.png')
            plt.close(fig)

    def plot_battery(self):
        if battery := self.data_series.get('battery'):
            fig, (ax_volt, ax_amp, ax_pct) = plt.subplots(1, 3, figsize=(12, 3.5))
            # Voltage
            ax_volt.set_title('Voltage')
            ax_volt.set_xlabel('mission time [s]')
            ax_volt.set_ylabel('voltage [V]')
            ax_volt.set_facecolor('#DDDDDD')
            ax_volt.plot(battery['time'], battery['voltage'], color='red')
            ax_volt.set_xlim(left=0)
            # Current
            ax_amp.set_title('Current')
            ax_amp.set_xlabel('mission time [s]')
            ax_amp.set_ylabel('current [A]')
            ax_amp.set_facecolor('#DDDDDD')
            ax_amp.plot(battery['time'], battery['current'], color='red')
            ax_amp.set_xlim(left=0)
            # Charge
            ax_pct.set_title('Charge')
            ax_pct.set_xlabel('mission time [s]')
            ax_pct.set_ylabel('remaining charge [%]')
            ax_pct.set_facecolor('#DDDDDD')
            ax_pct.plot(battery['time'], battery['charge'], color='red')
            ax_pct.get_yaxis().set_major_formatter(mpl.ticker.FuncFormatter(lambda x, p: f'{round(100*x)}'))
            ax_pct.set_xlim(left=0)
            #
            fig.tight_layout(pad=2)
            plt.savefig(self.graph_dir / 'battery.png')
            plt.close(fig)

    def plot_cpu(self):
        if cpu := self.data_series.get('cpu'):
            fig, (ax_temp, ax_util, ax_freq) = plt.subplots(1, 3, figsize=(12, 3.5))
            # Temperature
            ax_temp.set_title('Temperature')
            ax_temp.set_xlabel('mission time [s]')
            ax_temp.set_ylabel('core temperature [°C]')
            ax_temp.set_facecolor('#DDDDDD')
            ax_temp.plot(cpu['time'], cpu['temp'][0])
            ax_temp.plot(cpu['time'], cpu['temp'][1])
            ax_temp.plot(cpu['time'], cpu['temp'][2])
            ax_temp.plot(cpu['time'], cpu['temp'][3])
            ax_temp.set_xlim(left=0)
            # Utilization
            ax_util.set_title('Utilization')
            ax_util.set_xlabel('mission time [s]')
            ax_util.set_ylabel('core utilization [%]')
            ax_util.set_facecolor('#DDDDDD')
            ax_util.plot(cpu['time'], cpu['util'][0], label='cpu0')
            ax_util.plot(cpu['time'], cpu['util'][1], label='cpu1')
            ax_util.plot(cpu['time'], cpu['util'][2], label='cpu2')
            ax_util.plot(cpu['time'], cpu['util'][3], label='cpu3')
            ax_util.get_yaxis().set_major_formatter(mpl.ticker.FuncFormatter(lambda x, p: f'{round(100*x)}'))
            ax_util.set_xlim(left=0)
            ax_util.set_ylim(bottom=0)
            ax_util.legend(loc='lower right')
            # Frequency
            ax_freq.set_title('Clock frequency')
            ax_freq.set_xlabel('mission time [s]')
            ax_freq.set_ylabel('frequency [GHz]')
            ax_freq.set_facecolor('#DDDDDD')
            ax_freq.plot(cpu['time'], cpu['freq'][0])
            ax_freq.plot(cpu['time'], cpu['freq'][1])
            ax_freq.plot(cpu['time'], cpu['freq'][2])
            ax_freq.plot(cpu['time'], cpu['freq'][3])
            ax_freq.set_xlim(left=0)
            ax_freq.set_ylim(bottom=0)
            #
            fig.tight_layout(pad=2)
            plt.savefig(self.graph_dir / 'cpu.png')
            plt.close(fig)

    def plot_markers(self):
        if markers := self.calc_marker_visibility():
            fig, ax = plt.subplots(figsize=(10, 3.5))
            #
            ax.set_title('Visible markers')
            ax.set_xlabel('mission time [s]')
            ax.set_ylabel('no. of markers')
            ax.set_facecolor('#DDDDDD')
            ax.step(markers['time'], markers['count'], color='red', where='post')
            ax.set_xlim(left=0)
            ax.set_ylim(bottom=0)
            #
            fig.tight_layout(pad=2)
            plt.savefig(self.graph_dir / 'markers.png')
            plt.close(fig)

    def plot_route(self):
        corvus_pos = self.data_series.get('corvus_pos')
        voxl_pos = self.data_series.get('voxl_pos')
        if corvus_pos or voxl_pos:
            fig, ax = plt.subplots(figsize=(10, 10))
            #
            ax.set_xlabel('x [m]')
            ax.set_ylabel('y [m]')
            ax.set_aspect('equal')
            ax.set_facecolor('#DDDDDD')
            if corvus_pos:
                x = [x for x, _, _ in corvus_pos['pos']]
                y = [y for _, y, _ in corvus_pos['pos']]
                ax.plot(x, y, label='corvus')
            if voxl_pos:
                x = [x for x, _, _ in voxl_pos['pos']]
                y = [y for _, y, _ in voxl_pos['pos']]
                ax.plot(x, y, label='voxl')
            if markers := self.data_series.get('markers'):
                ax.plot(markers['x'], markers['y'], '.', label='marker')
            ax.legend()
            #
            plt.savefig(self.graph_dir / 'route.png')
            plt.close(fig)

    def write_report(self):
        def emit(line):
            target.write(line)
            target.write('\n')
        def html():
            emit("<!DOCTYPE html>")
            emit("<html>")
            head()
            body()
            emit("</html>")
        def head():
            emit( "  <head>")
            emit( "    <meta charset='utf-8'/>")
            emit(f"    <title>{self.key_values['drone_name']} : {self.key_values['flight_takeoff'].isoformat()}</title>")
            style()
            emit( "  </head>")
        def style():
            emit("    <style type='text/css'>")
            emit("      body { font-family: 'Times New Roman'; padding: 1em; background-color: white; }")
            emit("      h1, h2 { font-family: 'Verdana'; }")
            emit("      h1 { margin: 1em 0 2em 0; }")
            emit("      h2 { margin: 1em 0 0.25em 0; }")
            emit("      table { border-collapse: collapse; background-color: white; }")
            emit("      table { border: 1em solid white; }")
            emit("      td { padding-right: 2em; }")
            emit("      span.hide { color: grey; }")
            emit("    </style>")
        def body():
            emit( "  <body>")
            emit(f"    <h1>{self.key_values['drone_name']}<br>Flight {self.key_values['flight_takeoff'].replace(tzinfo=None)}</h1>")
            key_values()
            route_plot()
            time_series()
            emit( "  </body>")
        def key_values():
            # Determine takeoff moment in localtime
            takeoff_naive = self.key_values['flight_takeoff']
            takeoff_utc = takeoff_naive.replace(tzinfo=zoneinfo.ZoneInfo('UTC'))
            takeoff_cet = takeoff_utc.astimezone(tz=zoneinfo.ZoneInfo('CET'))
            takeoff_naive = takeoff_cet.replace(tzinfo=None)
            timezone = 'CEST' if takeoff_cet.dst() else 'CET'
            emit( "    <h2>Key values</h2>")
            emit( "    <table>")
            emit(f"      <tr><td>organization</td><td>{self.key_values['organization_name']}&emsp;<span class='hide'>{self.key_values['organization_id']}</span></td></tr>")
            emit(f"      <tr><td>location</td><td>{self.key_values['location_name']}&emsp;<span class='hide'>{self.key_values['location_id']}</span></td></tr>")
            emit(f"      <tr><td>drone</td><td>{self.key_values['drone_name']}&emsp;<span class='hide'>{self.key_values['drone_id']}</span></td></tr>")
            emit(f"      <tr><td>takeoff</td><td>{takeoff_naive} {timezone}</td></tr>")
            text = round(x) if (x := self.key_values['flight_duration']) else 'unknown'
            emit(f"      <tr><td>no. of images</td><td>{round(x) if (x := self.key_values['flight_duration']) else 'unknown'}</td></tr>")
            emit(f"      <tr><td>battery used</td><td>{round(-100 * x) if (x := self.key_values['battery_used']) else 'unknown'} %</td></tr>")
            emit(f"      <tr><td>max accuracy</td><td>{round(x, 2) if (x := self.key_values['max_accuracy']) else 'unknown'} m</td></tr>")
            emit(f"      <tr><td>max_cpu_temp</td><td>{self.key_values['max_cpu_temp']} °C</td></tr>")
            emit( "    </table>")
        def route_plot():
            if (png := self.analytics_dir / 'graphs' / 'route.png').exists():
                emit( "    <h2>Route</h2>")
                emit(f"    <img src='graphs/route.png'></img>")
        def time_series():
            if (png := self.analytics_dir / 'graphs' / 'alt_yaw.png').exists():
                emit( "    <h2>Altitude and yaw</h2>")
                emit(f"    <img src='graphs/alt_yaw.png'></img>")
            if (png := self.analytics_dir / 'graphs' / 'accuracy.png').exists():
                emit( "    <h2>Accuracy</h2>")
                emit(f"    <img src='graphs/accuracy.png'></img>")
            if (png := self.analytics_dir / 'graphs' / 'markers.png').exists():
                emit( "    <h2>Markers</h2>")
                emit(f"    <img src='graphs/markers.png'></img>")
            if (png := self.analytics_dir / 'graphs' / 'battery.png').exists():
                emit( "    <h2>Battery</h2>")
                emit(f"    <img src='graphs/battery.png'></img>")
            if (png := self.analytics_dir / 'graphs' / 'cpu.png').exists():
                emit( "    <h2>CPU</h2>")
                emit(f"    <img src='graphs/cpu.png'></img>")
        with open(self.analytics_dir / 'report.html', 'w') as target:
            html()
