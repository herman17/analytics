# Check the Upload directory periodically to see if new flight data
# has been uploaded by drones.

import time

from config import Configuration as cfg
from logger import Logger
from flightrep import FlightReport
from receiver import Receiver


class Watcher:

    def __init__(self, interval):
        self.interval = interval
        self.logger = Logger('watcher')
        self.logger.info('Starting the watcher', indent=0)

    def watch_forever(self):
        epoch = time.time()
        run_number = 0
        while True:
            # Check for new flight data
            self.logger.info(f'Run {run_number}', indent=0)
            self.check_uploads()
            run_number += 1
            # Wait until it's time for the next check
            next_time = epoch + run_number * self.interval
            pause = next_time - time.time()
            if pause > 0.0:
                time.sleep(pause)
            else:
                epoch -= pause

    def check_uploads(self):
        for drone_dir in cfg.upload_dir.glob('????????????????????'):
            for flight_dir in (drone_dir / 'logs').glob('????-??-??T??:??:??'):
                if (flight_dir / 'upload_done.txt').exists():
                    self.handle_flight(flight_dir)

    def handle_flight(self, flight_dir):
        drone_id = flight_dir.parent.parent.name
        flight_id = flight_dir.name
        self.logger.info(f'{drone_id} {flight_id}', indent=1)
        # Import the flight data into the telemetry data store.
        telemetry_dir = Receiver().receive(flight_dir)
        # Produce the HTML flight report for the new flight.
        FlightReport().report(telemetry_dir)


if __name__ == '__main__':
    watcher = Watcher(interval=5)
    watcher.watch_forever()
