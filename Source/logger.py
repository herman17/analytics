# A facility for low-frequency logging.

import datetime
import pathlib

from config import Configuration as cfg


# ANSI escape sequences
NORMAL = '\x1B[m'
RED =    '\x1B[91m'
YELLOW = '\x1B[93m'
GREEN =  '\x1B[92m'


class Logger:

    def __init__(self, topic):
        self.topic = topic
        self.dir = cfg.log_dir / topic
        self.dir.mkdir(parents=True, exist_ok=True)

    def info(self, message, indent):
        self.write(GREEN, f'I:  {indent*"  "}{message}')

    def warning(self, message):
        self.write(YELLOW, f'W:  {message}')

    def error(self, message):
        self.write(RED, f'E:  {message}')

    def write(self, color, message):
        date, time = datetime.datetime.now().isoformat(timespec='seconds').split('T')
        # To the terminal
        print(f'{time}  {color}{message}{NORMAL}')
        # To the log file
        logfile = self.dir / f'{date}.log'
        with open(logfile, 'a') as target:
            target.write(f'{time}  {message}\n')
