# Regenerate all flight reports, based on available telemetry data.
# This is useful after the content or layout of the flight report have changed.

import datetime

from config import Configuration as cfg
from logger import Logger
from flightrep import FlightReport


logger = Logger('regenerate')
logger.info('Regenerating all flight reports', indent=0)

# Swish the existing flight reports away
flights = cfg.analytics_dir / 'Flights'
if flights.exists():
    timestamp = datetime.datetime.now().isoformat(timespec='seconds')
    backup = flights.parent / f'Flights {timestamp}'
    flights.rename(backup)
    logger.warning(f'Existing flight reports moved to {backup}')

# Iterate over all telemetry data
count = 0
for drone_dir in sorted(cfg.telemetry_dir.glob('????????????????????')):
    drone_id = drone_dir.name
    logger.info(drone_id, indent=0)
    for workday_dir in sorted(drone_dir.glob('????-??-??')):
        workday = workday_dir.name
        logger.info(workday, indent=1)
        for flight_dir in sorted(workday_dir.glob('*')):
            flight_time = flight_dir.name
            logger.info(flight_time, indent=2)
            FlightReport().report(flight_dir)
            count += 1

# Done
logger.info(f'Regenerated {count} flight reports', indent=0)
