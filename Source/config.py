from pathlib import Path


class Configuration:

    # The root of all data
    data_dir = Path('/data').resolve()

    # Where drones upload flight data (temporarily)
    upload_dir = data_dir / 'Upload'
    # Where telemetry data is stored (permanently)
    telemetry_dir = data_dir / 'Telemetry'
    # Where analytics reports are written
    analytics_dir = data_dir / 'Analytics'

    # Where log files are written
    log_dir = data_dir / 'Log'
