import collections
import csv
import datetime
import json
import matplotlib as mpl
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import shutil

from config import Configuration as cfg


def dataset():
    def read_data(analytics_dir):
        return {dir.name: drone_data(dir) for dir in analytics_dir.glob('*')}
    def drone_data(drone_dir):
        return {dir.name: workday_data(dir) for dir in drone_dir.glob('*')}
    def workday_data(workday_dir):
        return {dir.name: flight_data(dir) for dir in workday_dir.glob('*')}
    def flight_data(flight_dir):
        return {
            'metadata': metadata_data(flight_dir / 'metadata.json'),
            'keyvalue': keyvalue_data(flight_dir / 'key_values.csv')
        }
    def metadata_data(metadata_file):
        if metadata_file.exists():
            return json.load(metadata_file)
    def keyvalue_data(keyvalue_file):
        if keyvalue_file.exists:
            with open(keyvalue_file) as source:
                reader = csv.reader(source)
                next(reader)  # Skip header
                return list(reader)
    return read_data(cfg.analytics_dir / 'Flights')


class DailyReport:

    def run(self):
        data = dataset()
        for report in self.__class__.__subclasses__():
            report().run(data)


class Dashboard(DailyReport):

    def run(self, dataset):
        self.dataset = dataset
        self.dashboard_dir = cfg.analytics_dir / 'Daily' / 'Dashboard'
        self.remove_old_dashboard()
        self.plot_graphs()
        self.generate_html()

    def remove_old_dashboard(self):
        if self.dashboard_dir.exists():
            shutil.rmtree(self.dashboard_dir)
        self.dashboard_dir.mkdir()
        (self.dashboard_dir / 'data').mkdir()
        (self.dashboard_dir / 'graphs').mkdir()

    def plot_graphs(self):
        # Cumulative number of flights per day
        flight_count = collections.Counter()
        for drone, workdays in self.dataset.items():
            for workday, flights in workdays.items():
                # workday = datetime.date.fromisoformat(workday)
                flight_count[workday] += len(flights)
        # Start the day before the first flight
        first = datetime.date.fromisoformat(min(flight_count))
        epoch = datetime.date.fromordinal(first.toordinal() - 1)
        date_series = [epoch]
        count_series = [0]
        total_flights = 0
        for workday in sorted(flight_count):
            total_flights += flight_count[workday]
            date_series.append(datetime.date.fromisoformat(workday))
            count_series.append(total_flights)
        fig, ax = plt.subplots(figsize=(6, 3.5))
        ax.set_title('Accumulated flights')
        ax.set_ylabel('number of flights')
        ax.set_facecolor('#DDDDDD')
        ax.xaxis.set_major_locator(mdates.MonthLocator())
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%b'))
        ax.plot(date_series, count_series, color='red')
        fig.tight_layout(pad=2)
        plt.savefig(self.dashboard_dir / 'graphs' / 'flights.png')

    def generate_html(self):
        def emit(line):
            target.write(line)
            target.write('\n')
        def html():
            emit("<!DOCTYPE html>")
            emit("<html>")
            head()
            body()
            emit("</html>")
        def head():
            emit( "  <head>")
            emit( "    <meta charset='utf-8'/>")
            emit(f"    <title>Dashboard</title>")
            style()
            emit( "  </head>")
        def style():
            emit("    <style type='text/css'>")
            emit("      body { font-family: 'Times New Roman'; padding: 1em; background-color: white; }")
            emit("      table { margin: 0 auto; border-collapse: collapse; }")
            emit("      td { padding: 1em; text-align: center; }")
            emit("    </style>")
        def body():
            emit("  <body>")
            emit("    <table>")
            emit("      <tr><td><img src='graphs/corvus-logo.png'></img></td></tr>")
            emit("      <tr><td><img src='graphs/flights.png'></img></td></tr>")
            emit("    </table>")
            emit( "  </body>")
        shutil.copy('/data/Stuff/corvus-logo.png', self.dashboard_dir / 'graphs')
        with open(self.dashboard_dir / 'dashboard.html', 'w') as target:
            html()


if __name__ == '__main__':
    DailyReport().run()
